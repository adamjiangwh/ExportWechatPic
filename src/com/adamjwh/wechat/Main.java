/**
 * @Title: Main.java
 * @Package com.adamjwh.wechat
 * @Description: 
 * @author adamjwh
 * @date 2018年7月29日
 * @version V1.0
 */
package com.adamjwh.wechat;

/**
 * @ClassName: Main
 * @Description: 
 * @author adamjwh
 * @date 2018年7月29日
 *
 */
public class Main {
	
	public static void main(String[] args) {
        ReplacementChain replacementChain = new ReplacementChain();
        replacementChain.addRegulation("", ".gif");
        Rename.multiRename("C:\\Users\\dell\\Desktop\\wechatfile", replacementChain);
	}

}
