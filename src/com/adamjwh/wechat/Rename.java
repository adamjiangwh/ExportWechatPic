/**
 * @Title: Rename.java
 * @Package com.adamjwh.wechat
 * @Description: 
 * @author adamjwh
 * @date 2018年7月29日
 * @version V1.0
 */
package com.adamjwh.wechat;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @ClassName: Rename
 * @Description: 重命名类
 * @author adamjwh
 * @date 2018年7月29日
 *
 */
public class Rename {

	public static void multiRename(String path,ReplacementChain replacementChain){
        File file = new File(path);
        boolean isDirectory = file.isDirectory();
 
        //如果不是文件夹，就返回
        if(!isDirectory){
            System.out.println(path + "不是一个文件夹！");
            return;
        }
 
        String[] files = file.list();
        File f = null;
        String filename = "";
        String oldFileName = "";
        
        //循环遍历所有文件
        for(String fileName : files){
            oldFileName = fileName;
            Map<String, String> map = replacementChain.getMap();
            for (Entry<String, String> entry : map.entrySet()) {
//            	fileName = fileName.replace(entry.getKey(), entry.getValue());
                fileName = fileName.concat(entry.getValue());
            }
 
            f = new File(path + "\\" + oldFileName); //输出地址和原路径保持一致
            f.renameTo(new File(path + "\\" +  fileName));
        }
        System.out.println("批量重命名成功！");
    }
	
}
