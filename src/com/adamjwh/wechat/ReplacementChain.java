/**
 * @Title: ReplacementChain.java
 * @Package com.adamjwh.wechat
 * @Description: 
 * @author adamjwh
 * @date 2018年7月29日
 * @version V1.0
 */
package com.adamjwh.wechat;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: ReplacementChain
 * @Description: 重命名规则类
 * @author adamjwh
 * @date 2018年7月29日
 *
 */
public class ReplacementChain {
	
	 private Map<String,String> map;
	 
    public ReplacementChain() {
        this.map = new HashMap<String, String>();
    }
 
    public Map<String, String> getMap() {
        return map;
    }
 
    // 添加新的替换规则(字符串替换)
    public ReplacementChain addRegulation(String oldStr, String newStr){
        this.map.put(oldStr, newStr);
        return this;
    }

}
