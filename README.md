# ExportWechatPic

#### 项目介绍
微信表情包在CustomEmotions文件夹下为没有后缀的文件，此代码简化了重命名过程，可以批量更改文件名，对微信CustomEmotions文件夹下的表情包自动批量添加“.gif”后缀。

#### 使用说明

1. 复制CustomEmotions文件夹中的文件到新建文件夹
2. 在Main类中修改文件夹路径为上述的新建文件夹的路径
3. 运行Main类即可